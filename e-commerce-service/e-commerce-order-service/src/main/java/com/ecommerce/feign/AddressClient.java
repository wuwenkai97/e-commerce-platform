package com.ecommerce.feign;

import com.ecommerce.account.AddressInfo;
import com.ecommerce.common.TableId;
import com.ecommerce.feign.hystrix.AddressClientHystrix;
import com.ecommerce.vo.CommonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户账户服务 Feign 接口定义(安全的)
 */
@FeignClient(
        contextId = "AddressClient",
        value = "e-commerce-account-service",
        fallback = AddressClientHystrix.class
)
public interface AddressClient {

    /**
     * 根据 id 查询地址信息
     */
    @RequestMapping(
            value = "/e-commerce-account-service/address/address-info/tableId",
            method = RequestMethod.POST
    )
    CommonResponse<AddressInfo> getAddressInfoByTableId(@RequestBody TableId tableId);

}
