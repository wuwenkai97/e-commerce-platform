package com.ecommerce.feign.hystrix;

import com.alibaba.fastjson.JSON;
import com.ecommerce.common.TableId;
import com.ecommerce.feign.SecuredGoodsClient;
import com.ecommerce.goods.SimpleGoodsInfo;
import com.ecommerce.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * 商品服务熔断降级兜底策略
 */
@Slf4j
@Component
public class GoodsClientHystrix implements SecuredGoodsClient {

    @Override
    public CommonResponse<List<SimpleGoodsInfo>> getSimpleGoodsInfoByTableId(TableId tableId) {
        log.error("[goods client feign request in order service] get simple goods error: [{}]",
                JSON.toJSONString(tableId));

        return new CommonResponse<>(
                -1,
                "[goods client feign request in order service]",
                Collections.emptyList()
        );
    }

}
