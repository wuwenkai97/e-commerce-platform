package com.ecommerce.feign.hystrix;

import com.alibaba.fastjson.JSON;
import com.ecommerce.account.AddressInfo;
import com.ecommerce.common.TableId;
import com.ecommerce.feign.AddressClient;
import com.ecommerce.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * 账户服务熔断降级兜底策略
 */
@Slf4j
@Component
public class AddressClientHystrix implements AddressClient {

    @Override
    public CommonResponse<AddressInfo> getAddressInfoByTableId(TableId tableId) {

        log.error("[account client feign request error in order service] get address info error: [{}]",
                JSON.toJSONString(tableId));
        return new CommonResponse<>(
                -1,
                "[account client feign request error in order service]",
                new AddressInfo(-1L, Collections.emptyList())
        );
    }
}
