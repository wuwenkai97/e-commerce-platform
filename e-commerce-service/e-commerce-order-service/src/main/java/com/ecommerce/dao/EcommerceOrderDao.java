package com.ecommerce.dao;

import com.ecommerce.entity.EcommerceOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * EcommerceOrder Dao 接口定义
 */
public interface EcommerceOrderDao extends PagingAndSortingRepository<EcommerceOrder, Long> {

    // 根据 userId 查询分页订单
    Page<EcommerceOrder> findAllByUserId(Long userId, Pageable pageable);

}
