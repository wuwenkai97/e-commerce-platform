package com.ecommerce;

import com.ecommerce.config.DataSourceProxyAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * 订单微服务启动入口
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableJpaAuditing
@EnableFeignClients
@EnableCircuitBreaker
@Import(DataSourceProxyAutoConfiguration.class)
public class EcommerceOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommerceOrderApplication.class, args);
    }

}
