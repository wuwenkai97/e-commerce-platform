package com.ecommerce.service;

import com.ecommerce.common.TableId;
import com.ecommerce.order.OrderInfo;
import com.ecommerce.vo.PageSimpleOrderDetail;
import io.seata.spring.annotation.GlobalTransactional;

/**
 * 订单相关服务接口定义
 */

public interface OrderService {

    /**
     * 下单(分布式事务)：创建订单、扣减库存、扣减余额、创建物流信息(Stream + Kafka)
     */
    TableId createOrder(OrderInfo orderInfo);

    /**
     * 获取当前用户的订单信息：带有分页
     */
    PageSimpleOrderDetail getSimpleOrderDetailByPage(int page);
}
