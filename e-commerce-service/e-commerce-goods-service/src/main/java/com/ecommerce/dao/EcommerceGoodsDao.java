package com.ecommerce.dao;

import com.ecommerce.constant.BrandCategory;
import com.ecommerce.constant.GoodsCategory;
import com.ecommerce.entity.EcommerceGoods;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * EcommerceGoodsDao接口定义
 */
public interface EcommerceGoodsDao extends PagingAndSortingRepository<EcommerceGoods, Long> {

    /**
     * 根据查询条件查询商品表并限制, 返回结果
     */
    Optional<EcommerceGoods> findFirst1ByGoodsCategoryAndBrandCategoryAndGoodsName(
            GoodsCategory goodsCategory,
            BrandCategory brandCategory,
            String goodsName
    );

}
