package com.ecommerce;

import com.ecommerce.config.DataSourceProxyAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * 商品微服务启动入口
 * 启动依赖组件：Redis + Mysql + Kafka + Zipkin
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableJpaAuditing
@Import(DataSourceProxyAutoConfiguration.class)
public class GoodsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodsApplication.class, args);
    }

}
