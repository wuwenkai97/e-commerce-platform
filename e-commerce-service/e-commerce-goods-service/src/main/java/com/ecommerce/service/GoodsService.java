package com.ecommerce.service;

import com.ecommerce.common.TableId;
import com.ecommerce.goods.GoodsInfo;
import com.ecommerce.goods.SimpleGoodsInfo;
import com.ecommerce.goods.DeductGoodsInventory;
import com.ecommerce.vo.PageSimpleGoodsInfo;

import java.util.List;

/**
 * 商品微服务相关服务接口定义
 */
public interface GoodsService {

    /**
     * 根据 TableId 查询商品详情信息
     */
    List<GoodsInfo> getGoodsInfoByTableId(TableId tableId);

    /**
     * 获取分页的商品信息
     */
    PageSimpleGoodsInfo getSimpleGoodsInfoByPage(int page);

    /**
     * 根据 TableId 查询简单商品信息
     */
    List<SimpleGoodsInfo> getSimpleGoodsInfoByTableId(TableId tableId);

    /**
     * 扣减商品库存
     */
    Boolean deductGoodsInventory(List<DeductGoodsInventory> deductGoodsInventories);
}
