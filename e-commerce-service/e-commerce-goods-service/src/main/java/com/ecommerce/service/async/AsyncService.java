package com.ecommerce.service.async;

import com.ecommerce.goods.GoodsInfo;

import java.util.List;

/**
 * 异步服务接口定义
 */
public interface AsyncService {

    /**
     * 异步将商品信息保存
     */
    void asyncImportGoods(List<GoodsInfo> goodsInfos, String taskId);

}
