package com.ecommerce.converter;

import com.ecommerce.constant.GoodsCategory;

import javax.persistence.AttributeConverter;

/**
 * 商品类别枚举类转换器
 */
public class GoodsCategoryConverter implements AttributeConverter<GoodsCategory, String> {
    @Override
    public String convertToDatabaseColumn(GoodsCategory goodsCategory) {
        return goodsCategory.getCode();
    }

    @Override
    public GoodsCategory convertToEntityAttribute(String code) {
        return GoodsCategory.of(code);
    }
}
