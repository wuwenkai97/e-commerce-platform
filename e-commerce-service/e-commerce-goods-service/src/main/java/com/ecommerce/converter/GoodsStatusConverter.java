package com.ecommerce.converter;

import com.ecommerce.constant.GoodsStatus;

import javax.persistence.AttributeConverter;

/**
 * 商品状态枚举属性转换器
 */
public class GoodsStatusConverter implements AttributeConverter<GoodsStatus, Integer> {

    /**
     * 转换为数据表字段
     */
    @Override
    public Integer convertToDatabaseColumn(GoodsStatus goodsStatus) {
        Integer status = goodsStatus.getStatus();
        return status;
    }

    /**
     * 将数据表字段转换为实体类
     */
    @Override
    public GoodsStatus convertToEntityAttribute(Integer status) {

        return GoodsStatus.of(status);
    }
}
