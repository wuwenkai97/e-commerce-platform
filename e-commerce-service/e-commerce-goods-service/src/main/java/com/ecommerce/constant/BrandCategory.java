package com.ecommerce.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * 商品品牌枚举类
 */
@Getter
@AllArgsConstructor
public enum BrandCategory {

    BrandA("20001", "品牌A"),
    BrandB("20002", "品牌B"),
    BrandC("20003", "品牌C"),
    BrandD("20004", "品牌D"),
    BrandE("20005", "品牌E"),
    ;

    // 品牌分类编码
    private final String code;

    // 品牌分类描述
    private final String description;

    /**
     * 根据code 获取 BrandCategory
     */
    public static BrandCategory of(String code) {
        Objects.requireNonNull(code);

        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElseThrow(
                        () -> new IllegalArgumentException(code + " not exists")
                );
    }
}
