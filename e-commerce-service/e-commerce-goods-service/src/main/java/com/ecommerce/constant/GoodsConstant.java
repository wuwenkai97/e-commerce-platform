package com.ecommerce.constant;

/**
 * 商品常量信息
 */
public class GoodsConstant {

    // redis Key
    public static final String ECOMMERCE_GOODS_DICT_KEY = "ecommerce:goods:dict:20210925";

}
