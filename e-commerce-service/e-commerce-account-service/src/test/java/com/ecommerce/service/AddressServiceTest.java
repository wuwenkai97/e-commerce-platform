package com.ecommerce.service;

import com.alibaba.fastjson.JSON;
import com.ecommerce.account.AddressInfo;
import com.ecommerce.common.TableId;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

/**
 * 用户地址相关服务功能测试
 */
@Slf4j
public class AddressServiceTest extends BaseTest{

    @Autowired
    private AddressService addressService;

    /**
     * 测试创建用户地址信息
     */
    @Test
    public void testCreateAddressInfo() {
        AddressInfo.AddressItem addressItem = new AddressInfo.AddressItem();
        addressItem.setUsername("wen");
        addressItem.setPhone("123456789");
        addressItem.setProvince("浙江省");
        addressItem.setCity("杭州市");
        addressItem.setAddressDetail("西湖区");

        log.info("test create address info: [{}]", JSON.toJSONString(
                addressService.createAddressInfo(
                        new AddressInfo(loginUserInfo.getId(),
                                Collections.singletonList(addressItem))
                )
        ));
    }

    /**
     * 测试获取当前登录用户地址信息
     */
    @Test
    public void testGetCurrentAddressInfo() {
        log.info("test get current user info: [{}]", JSON.toJSONString(
                addressService.getCurrentAddressInfo()
        ));
    }

    /**
     * 根据id获取地址信息
     */
    @Test
    public void testGetAddressInfoById() {
        log.info("test get address info by id: [{}]", JSON.toJSONString(
                addressService.getAddressInfoById(10L)
        ));
    }

    /**
     * 根据TableId查询AddressInfo
     */
    @Test
    public void testGetAddressInfoByTableId() {
        log.info("test get address info by tableId: [{}]", JSON.toJSONString(
                addressService.getAddressInfoByTableId(
                        new TableId(Collections.singletonList(new TableId.Id(10L)))
                )
        ));
    }
}
