package com.ecommerce.controller;

import com.ecommerce.account.AddressInfo;
import com.ecommerce.common.TableId;
import com.ecommerce.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 用户地址服务 Controller
 */
@Api(tags = "用户地址服务")
@RestController
@Slf4j
@RequestMapping("/address")
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @RequestMapping(value = "/create-address", method = RequestMethod.POST)
    @ApiOperation(value = "创建", notes = "创建用户地址信息", httpMethod = "POST")
    public TableId createAddressInfo(@RequestBody AddressInfo addressInfo) {
        return addressService.createAddressInfo(addressInfo);
    }

    @ApiOperation(value = "当前用户", notes = "获取当前登录用户地址信息", httpMethod = "GET")
    @RequestMapping(value = "/current-address", method = RequestMethod.GET)
    public AddressInfo getCurrentAddressInfo() {
        return addressService.getCurrentAddressInfo();
    }

    @ApiOperation(
            value = "获取用户地址",
            notes = "通过id获取用户地址信息, id 为 EcommerceAddress 表的主键",
            httpMethod = "GET")
    @RequestMapping(value = "/address-info/id", method = RequestMethod.GET)
    public AddressInfo getAddressInfoById(@RequestParam("id") Long id) {
        return addressService.getAddressInfoById(id);
    }

    @ApiOperation(
            value = "获取用户地址",
            notes = "通过 tableId 获取用户地址信息",
            httpMethod = "POST")
    @RequestMapping(value = "/address-info/tableId", method = RequestMethod.POST)
    public AddressInfo getAddressInfoByTableId(@RequestBody TableId tableId) {
        return addressService.getAddressInfoByTableId(tableId);
    }

}
