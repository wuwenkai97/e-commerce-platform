package com.ecommerce.controller;

import com.ecommerce.account.BalanceInfo;
import com.ecommerce.service.BalanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户余额服务 Controller
 */
@Api(tags = "用户余额服务")
@RestController
@Slf4j
@RequestMapping("/balance")
public class BalanceController {

    private final BalanceService balanceService;

    public BalanceController(BalanceService balanceService) {
        this.balanceService = balanceService;
    }

    @ApiOperation(value = "当前用户", notes = "获取当前登录用户账户信息", httpMethod = "GET")
    @RequestMapping("/current-balance")
    public BalanceInfo getCurrentUserBalanceInfo() {
        return balanceService.getCurrentUserBalanceInfo();
    }

    @ApiOperation(value = "扣减", notes = "扣钱当前用户账户余额", httpMethod = "PUT")
    @RequestMapping(value = "/deduct-balance", method = RequestMethod.PUT)
    public BalanceInfo deductBalance(@RequestBody BalanceInfo balanceInfo) {
        return balanceService.deductBalance(balanceInfo);
    }
}
