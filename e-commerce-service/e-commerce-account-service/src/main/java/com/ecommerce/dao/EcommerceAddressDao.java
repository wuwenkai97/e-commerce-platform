package com.ecommerce.dao;

import com.ecommerce.entity.EcommerceAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * EcommerceAddressDao接口定义
 */
public interface EcommerceAddressDao extends JpaRepository<EcommerceAddress, Long> {

    /**
     * 根据用户id查询当前用户所有地址信息
     */
    List<EcommerceAddress> findAllByUserId(Long userId);

}
