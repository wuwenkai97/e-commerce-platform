package com.ecommerce.dao;

import com.ecommerce.entity.EcommerceBalance;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Ecommerce Balance接口定义
 */
public interface EcommerceBalanceDao extends JpaRepository<EcommerceBalance, Long> {

    // 根据userId 查询 EcommerceBalance 对象
    EcommerceBalance findByUserId(Long userId);



}
