package com.ecommerce.service;

import com.ecommerce.account.BalanceInfo;

/**
 * 用于余额相关的服务接口定义
 */
public interface BalanceService {

    /**
     * 获取当前用户余额信息
     */
    BalanceInfo getCurrentUserBalanceInfo();

    /**
     * 扣减用户余额
     * @param balanceInfo 代表想要扣减的余额, 并不是真实的余额
     */
    BalanceInfo deductBalance(BalanceInfo balanceInfo);

}
