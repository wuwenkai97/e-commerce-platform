package com.ecommerce.service;

import com.ecommerce.account.AddressInfo;
import com.ecommerce.common.TableId;

/**
 * 用户地址相关服务接口定义
 */
public interface AddressService {

    /**
     * 创建用户地址信息
     */
    TableId createAddressInfo(AddressInfo addressInfo);

    /**
     * 获取当前登录的用户地址信息
     */
    AddressInfo getCurrentAddressInfo();

    /**
     * 通过 id 获取用户地址信息，id是 EcommerceAddress 表的主键
     */
    AddressInfo getAddressInfoById(Long id);

    /**
     * 通过 TableId 获取用户地址信息
     */
    AddressInfo getAddressInfoByTableId(TableId tableId);
}
