package com.ecommerce.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建订单时发送的物流消息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Stream 物流消息对象")
public class LogisticsMessage {

    @ApiModelProperty(value = "用户表主键 id")
    private Long userId;

    @ApiModelProperty(value = "订单表主键 id")
    private Long orderId;

    @ApiModelProperty(value = "用户地址表主键 id")
    private Long addressId;

    @ApiModelProperty(value = "备注信息(json 信息)")
    private String extraInfo;
}
