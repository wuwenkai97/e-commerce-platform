package com.ecommerce.order;

import com.ecommerce.goods.DeductGoodsInventory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 订单信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "用户发起购买订单")
public class OrderInfo {

    @ApiModelProperty(value = "用户地址表主键 id")
    private Long userAddressId;

    @ApiModelProperty(value = "订单中的商品信息")
    private List<OrderItem> orderItems;

    /**
     * 订单中的商品信息
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ApiModel(description = "订单中的单项商品信息")
    public static class OrderItem {

        @ApiModelProperty(value = "商品表主键 Id")
        private Long goodsId;

        @ApiModelProperty(value = "商品购买个数")
        private Integer count;

        public DeductGoodsInventory toDeductGoodsInventory() {
            return new DeductGoodsInventory(this.goodsId, this.count);
        }
    }

}
