package com.ecommerce.account;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 用户地址信息
 */
@ApiModel(description = "用户地址信息")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressInfo {

    @ApiModelProperty(value = "地址所属用户 Id")
    private Long userId;

    @ApiModelProperty(value = "地址详情信息")
    private List<AddressItem> addressItems;

    @ApiModel(description = "用户单个地址信息")
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddressItem {

        @ApiModelProperty(value = "地址表主键 Id")
        private Long id;

        @ApiModelProperty(value = "用户姓名")
        private String username;

        @ApiModelProperty(value = "电话")
        private String phone;

        @ApiModelProperty(value = "省")
        private String province;

        @ApiModelProperty(value = "市")
        private String city;

        @ApiModelProperty(value = "详细地址")
        private String addressDetail;

        @ApiModelProperty(value = "创建时间")
        private Date creatTime;

        @ApiModelProperty(value = "更新时间")
        private Date updateTime;

        public AddressItem(Long id) {
            this.id = id;
        }

        /**
         * 将AddressItem转换为UserAddress
         */
        public UserAddress toUserAddress() {
            UserAddress userAddress = new UserAddress();
            userAddress.setUsername(username);
            userAddress.setPhone(phone);
            userAddress.setProvince(province);
            userAddress.setCity(city);
            userAddress.setAddressDetail(addressDetail);
            return userAddress;
        }
    }
}
