package com.ecommerce.dao;

import com.ecommerce.entity.EcommerceLogistics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EcommerceLogisticsDao extends JpaRepository<EcommerceLogistics, Long> {
}
