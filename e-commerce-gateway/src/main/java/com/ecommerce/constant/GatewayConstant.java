package com.ecommerce.constant;

/**
 * 网关常量
 */
public class GatewayConstant {

    // 登录uri
    public static final String LOGIN_URI = "/e-commerce/login";

    // 注册 uri
    public static final String REGISTER_URI = "/e-commerce/register";

    // 去授权中心拿到登录Token的uri格式化接口
    public static final String AUTHORITY_CENTER_TOKEN_URL_FORMAT =
            "http://%s:%s/e-commerce-authority-center/authority/token";

    // 去授权中心注册并拿到 token 的 uri 格式化接口
    public static final String AUTHORITY_CENTER_REGISTER_URL_FORMAT =
            "http://%s:%s/e-commerce-authority-center/authority/register";

}
