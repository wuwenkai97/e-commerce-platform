package com.ecommerce.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * 通过Nacos下发动态路由配置, 监听 Nacos 中路由配置变更
 */
@Slf4j
@Component
@DependsOn({"gatewayConfig"}) // GatewayConfig加载以后再加载本类
public class DynamicRouteServiceImplByNacos {

    // Nacos 配置服务
    private ConfigService configService;

    private final DynamicRouteServiceImpl dynamicRouteService;

    public DynamicRouteServiceImplByNacos(DynamicRouteServiceImpl dynamicRouteService) {
        this.dynamicRouteService = dynamicRouteService;
    }

    /**
     * 初始化 Nacos Config
     */
    public ConfigService initConfigService() {
        try {
            Properties properties = new Properties();
            properties.setProperty("serverAddr", GatewayConfig.NACOS_SERVER_ADDR);
            properties.setProperty("namespace", GatewayConfig.NACOS_NAMESPACE);
            return configService = NacosFactory.createConfigService(properties);
        }catch (Exception exception) {
            log.error("init gateway nacos config error: [{}]", exception.getMessage(), exception);
            return null;
        }
    }

    /**
     * 监听 Nacos 下发的动态路由配置
     */
    private void dynamicRouteByNacosListener(String dataId, String group) {
        try {
            // 给Nacos Config 客户端增加一个监听器
            configService.addListener(dataId, group, new Listener() {
                /**
                 * 自己提供线程池执行操作
                 */
                @Override
                public Executor getExecutor() {
                    return null;
                }

                /**
                 * 监听器收到配置更新
                 * @param configInfo Nacos中最新的配置定义
                 */
                @Override
                public void receiveConfigInfo(String configInfo) {
                    log.info("start to update config: [{}]", configInfo);
                    List<RouteDefinition> definitionList =
                            JSON.parseArray(configInfo, RouteDefinition.class);
                    log.info("update route: [{}]", definitionList.toString());
                    dynamicRouteService.updateList(definitionList);
                }
            });
        }catch (NacosException exception) {
            log.error("dynamic update gateway config error: [{}]", exception.getMessage(), exception);
        }
    }

    /**
     * 连接Nacos之后的初始化;Bean再容器中构造完成之后会执行此方法
     */
    @PostConstruct
    public void init() {
        log.info("gateway route init ...");

        try {
            // 初始化 Ncos 配置客户端
            configService = initConfigService();
            if (configService == null) {
                log.error("init config service fail");
                return;
            }
            // 通过 Nacos Config 并指定路由配置路径去获取路由配置
            String configInfo = configService.getConfig(
                    GatewayConfig.NACOS_ROUTE_DATA_ID,
                    GatewayConfig.NACOS_ROUTE_GROUP_ID,
                    GatewayConfig.DEFAULT_TIMEOUT
            );

            log.info("get current gateway config: [{}]", configInfo);
            List<RouteDefinition> definitionList =
                    JSON.parseArray(configInfo, RouteDefinition.class);
            if (!CollectionUtils.isEmpty(definitionList)) {
                for (RouteDefinition routeDefinition: definitionList) {
                    log.info("init gateway config: [{}]", routeDefinition.toString());
                    dynamicRouteService.addRouteDefinition(routeDefinition);
                }
            }
        }catch (Exception exception) {
            log.error("gateway route init has some error: [{}]", exception.getMessage(), exception);
        }

        dynamicRouteByNacosListener(GatewayConfig.NACOS_ROUTE_DATA_ID, GatewayConfig.NACOS_ROUTE_GROUP_ID);
    }
}
