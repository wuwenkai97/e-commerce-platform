package com.ecommerce;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Gateway环境测试
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class GatewayApplicationTest {

    @Test
    public void contextLoad() {

    }

}
