package com.ecommerce.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 授权中心鉴权之后给客户端的Token
 * 初始状态只有token一个属性, 但考虑后期系统的扩展性(如创建用户等信息的加入), 需要专门设置一个对象存储授权响应信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtToken {

    // JWT
    private String token;

}
