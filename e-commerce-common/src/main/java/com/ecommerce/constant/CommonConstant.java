package com.ecommerce.constant;

/**
 * 通用常量
 */
public final class CommonConstant {

    // RSA公钥
    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk" +
            "YOpQ6dKXkLJxSKTAkVaF/r+Rblpajuw9PCbjz6DKEwXT2g6d6CxDJifEwb1XiieVHdTnUw34xyaCS" +
            "ksQB6RUmFxKKZYqGPFrs2AeCOzE3yThWdaulcM2e1IQNAQNepsJiYHri5qppAWXup05xo8yJT1wE4" +
            "fKpy+waJmvQ60MJxzBzp7uRiVmBYSXZKdvsMMJKVUllSeBGc6NZ5xt1KV6qaNixz37Oq4ubYXm3ri" +
            "gtkyMvvCjhHskaTgjsFmJza+z66453FejePWZqukpykNnprX5FJHYvtrRZtN4TjOqWE5xA655Q0FX" +
            "yFI63q2efRomCQSfrwd1IfrH0QU61bUiQIDAQAB";

    // JWT中存储用户信息的 key
    public static final String JWT_USER_INFO_KEY = "e-commerce-user";

    // 授权中心的 service-id
    public static final String AUTHORITY_CENTER_SERVICE_ID = "e-commerce-authority-center";
}
