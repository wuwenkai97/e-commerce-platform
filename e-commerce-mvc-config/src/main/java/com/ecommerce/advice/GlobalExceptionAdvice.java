package com.ecommerce.advice;

import com.ecommerce.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常捕获处理
 */
@Slf4j
@RestControllerAdvice(value = "com.ecommerce")
public class GlobalExceptionAdvice {

    @ExceptionHandler(value = Exception.class)
    public CommonResponse<String> handlerCommerceException(
        HttpServletRequest request,
        Exception exception
    ) {
        CommonResponse<String> response = new CommonResponse<>(
          -1, "commerce error"
        );
        response.setData(exception.getMessage());
        log.error("commerce service has error: [{}]", exception.getMessage(), exception);
        return response;
    }
}
