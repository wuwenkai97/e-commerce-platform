# 电商平台解决方案

## 项目简介

这一一个基于SpringCloud Alibaba框架开发的一个电商工程，主要目的是从真实业务场景出发，落地微服务组件的集成和应用工程编码、部署、测试流程与规范。工程主要分为商品服务、地址服务、用户账户服务以及物流服务模块。

## 技术栈

* 后端：SpringBoot + SpringCloud + JPA
* 数据库：MySQL + Redis
* 消息中间件：Kafka
* 工具：Sentinel + Zipkin + Seata

## 工程结构

项目总共拥有10个子模块，分别为e-commerce-admin、e-commerce-alibaba-nacos-client、e-commerce-authority-center、e-commerce-common、e-commerce-gateway、e-commerce-hystrix-dashboard、e-commerce-mvc-config、e-commerce-sentinel-client、e-commerce-service、e-commerce-stream-client，其中各模块主要功能如下：

* e-commerce-mvc-config：提供了全局统一响应处理机制以及统一拦截器，并提供了忽略响应处理的注解
* e-commerce-common：提供公共的 vo 如用户信息、token信息等等，还提供了 Jwt 解析工具
* e-commerce-alibaba-nacos-client：用于提供工程验证，不属于线上的功能模块。在该模块中主要验证了 Nacos、Hystrix、Sleuth、Feign及Ribbon等组件
* e-commerce-admin：集成了 Spring Security 框架，对所有请求进行安全认证；提供了报警机制，实例状态改变会进行通知
* e-commerce-authority-center：主要提供授权功能，即 token 的生成
* e-commerce-gateway：网关模块，提供了动态路由更新机制（即Nacos中修改了路由配置，本地会打印相关日志信息），提供了全局过滤器完成鉴权、计时等工作
* e-commerce-service：业务模块，主要包含 用户账户、商品、订单、物流 四个子模块
* e-commerce-hystrix-dashboard：hystrix控制台模块
* e-commerce-stream-client：消息通信模块
* e-commerce-sentinel-client：限流控制台模块

## 技术亮点

1. 采用领域驱动设计（DDD）的思想进行建模，提高系统的可扩展性
2. 利用 Jwt 机制完成授权鉴权，即服务器收到用户请求后与数据库中的用户信息对比，若对比成功，则利用私钥加密用户信息并生成 Jwt，Jwt 的优势在于存储在 Header 中，可扩展性强，相比较下，session需要做多机数据共享。
3. 实现了拦截器，并在拦截器内利用 ThreadLocal 单独存储每一个线程的用户信息
4. 实现了商品异步入库，步骤如下：
   * 自定义线程池
   * 实现异步入库接口
   * 定义异步监控切面，设置异步任务执行状态
5. 利用 Seata 实现分布式事务管理，主要用于订单创建功能，因为在订单创建过程中存在商品扣减，用户账户余额扣减以及物流消息传递

