package com.ecommerce.service.communication;

import feign.Logger;
import feign.Request;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Open-Feign 配置类
 */
@Configuration
public class FeignConfig {

    /**
     * 开启Open-Feign日志
     */
    @Bean
    public Logger.Level feignLogger() {
        return Logger.Level.FULL; // 需要注意, 工程日志级别需要修改成debug
    }

    /**
     * Open-Feign 开启重试
     */
    @Bean
    public Retryer feignRetryer() {
        return new Retryer.Default(
                100, // period = 100 发起当前请求的时间间隔, 单位是ms
                SECONDS.toMillis(1), // maxPeriod, 发起当前请求的最大时间间隔, 单位是ms
                5 // 最大尝试次数
        );
    }

    public static final int CONNECT_TIMEOUT_MILLS = 5000;
    public static final int READ_TIMEOUT_MILLS = 5000;

    /**
     * 对请求的连接和响应时间进行限制
     */
    public Request.Options options() {
        return new Request.Options(
                CONNECT_TIMEOUT_MILLS, TimeUnit.MICROSECONDS,
                READ_TIMEOUT_MILLS, TimeUnit.MILLISECONDS,
                true // 转发也进行设置
        );
    }
}
