package com.ecommerce.service.communication;

import com.ecommerce.service.communication.hystrix.AuthorityFeignClientFallback;
import com.ecommerce.service.communication.hystrix.AuthorityFeignClientFallbackFactory;
import com.ecommerce.vo.JwtToken;
import com.ecommerce.vo.UsernameAndPassword;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 与 Authority 服务通信的 Feign Client 接口定义
 */
@FeignClient(
        contextId = "AuthorityFeignClient",
        value = "e-commerce-authority-center",
//        fallback = AuthorityFeignClientFallback.class
        fallbackFactory = AuthorityFeignClientFallbackFactory.class
)
public interface AuthorityFeignClient {

    /**
     * 通过 OpenFeign 访问 Authority 获取 Token
     */
    @RequestMapping(value = "/e-commerce-authority-center/authority/token", method = RequestMethod.POST,
            consumes = "application/json", produces = "application/json")
    JwtToken getTokenByFeign(@RequestBody UsernameAndPassword usernameAndPassword);

}
