package com.ecommerce.controller;

import com.ecommerce.service.communication.AuthorityFeignClient;
import com.ecommerce.service.communication.UseFeignApi;
import com.ecommerce.service.communication.UseRestTemplateService;
import com.ecommerce.service.communication.UseRibbonService;
import com.ecommerce.vo.JwtToken;
import com.ecommerce.vo.UsernameAndPassword;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微服务通信 Controller
 */
@RestController
@RequestMapping("/communication")
public class CommunicationController {

    private final UseRestTemplateService useRestTemplateService;

    private final UseRibbonService useRibbonService;

    private final AuthorityFeignClient authorityFeignClient;

    private final UseFeignApi useFeignApi;

    public CommunicationController(UseRestTemplateService useRestTemplateService, UseRibbonService useRibbonService, AuthorityFeignClient authorityFeignClient, UseFeignApi useFeignApi) {
        this.useRestTemplateService = useRestTemplateService;
        this.useRibbonService = useRibbonService;
        this.authorityFeignClient = authorityFeignClient;
        this.useFeignApi = useFeignApi;
    }

    @RequestMapping(value = "/rest-template", method = RequestMethod.POST)
    public JwtToken getTokenFromAuthorityService(@RequestBody UsernameAndPassword usernameAndPassword) {
        return useRestTemplateService.getTokenFromAuthorityService(usernameAndPassword);
    }

    @RequestMapping(value = "/rest-template-load-balance", method = RequestMethod.POST)
    public JwtToken getTokenFromAuthorityServiceWithLoadBalance(@RequestBody UsernameAndPassword usernameAndPassword) {
        return useRestTemplateService.getTokenFromAuthorityServiceWithLoadBalance(usernameAndPassword);
    }

    @RequestMapping(value = "/ribbon", method = RequestMethod.POST)
    public JwtToken getTokenFromAuthorityServiceByRibbon(@RequestBody UsernameAndPassword usernameAndPassword) {
        return useRibbonService.getTokenFromAuthorityServiceByRibbon(usernameAndPassword);
    }

    @RequestMapping("/thinking-in-ribbon")
    public JwtToken thinkingInRibbon(@RequestBody UsernameAndPassword usernameAndPassword) {
        return useRibbonService.thinkingInRibbon(usernameAndPassword);
    }

    @RequestMapping("/token-by-feign")
    public JwtToken getTokenByFeign(@RequestBody UsernameAndPassword usernameAndPassword) {
        return authorityFeignClient.getTokenByFeign(usernameAndPassword);
    }

    @RequestMapping("/thinking-in-feign")
    public JwtToken thinkingInFeign(@RequestBody UsernameAndPassword usernameAndPassword) {
        return useFeignApi.thinkingInFeign(usernameAndPassword);
    }
}
