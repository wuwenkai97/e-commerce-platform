package com.ecommerce.servie;

import com.ecommerce.vo.UsernameAndPassword;

/**
 * JWT相关服务接口定义
 */
public interface JWTService {

    /**
     * 生成JWT, 使用默认的超时时间
     */
    String generateToken(String username, String password) throws Exception;

    /**
     * 生成JWT, 指定超时时间(单位：天)
     */
    String generateToken(String username, String password, int expire) throws Exception;

    /**
     * 注册用户并生成 Token 返回
     */
    String registerUserAndGenerateToken(UsernameAndPassword usernameAndPassword) throws Exception;

}
