package com.ecommerce.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户表实体类定义
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class) // 监听器，EcommerceUser这张表可以帮助我们自动更新创建时间、更新时间
@SuppressWarnings("all")
@Table(name = "t_ecommerce_user")
public class EcommerceUser implements Serializable {

    // 自增主键
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 自增的方式
    @Column(name = "id", nullable = false)
    private Long id;

    // 用户名
    @Column(name = "username", nullable = false)
    private String username;

    // MD5 密码
    @Column(name = "password", nullable = false)
    private String password;

    // 额外信息，json字符串存储
    @Column(name = "extra_info", nullable = false)
    private String extraInfo;

    // 创建时间
    @CreatedDate // JPA自动审计
    @Column(name = "create_time", nullable = false)
    private Date creatTime;

    // 更新时间
    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private Date updateTime;

}
