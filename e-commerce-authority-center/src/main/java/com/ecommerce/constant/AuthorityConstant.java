package com.ecommerce.constant;

/**
 * 授权需要使用的常量信息
 */
public final class AuthorityConstant {

    // RSA私钥 除了授权中心以外，不暴露给任何的客户端
    public static final String PRIVATE_KEY = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEA" +
            "AoIBAQCRg6lDp0peQsnFIpMCRVoX+v5FuWlqO7D08JuPPoMoTBdPaDp3oLEMmJ8TBvVeKJ5Ud1OdT" +
            "DfjHJoJKSxAHpFSYXEoplioY8WuzYB4I7MTfJOFZ1q6VwzZ7UhA0BA16mwmJgeuLmqmkBZe6nTnGj" +
            "zIlPXATh8qnL7Boma9DrQwnHMHOnu5GJWYFhJdkp2+wwwkpVSWVJ4EZzo1nnG3UpXqpo2LHPfs6ri" +
            "5thebeuKC2TIy+8KOEeyRpOCOwWYnNr7PrrjncV6N49Zmq6SnKQ2emtfkUkdi+2tFm03hOM6pYTnE" +
            "DrnlDQVfIUjrerZ59GiYJBJ+vB3Uh+sfRBTrVtSJAgMBAAECggEBAIKzQKicrWanFLY1zNegVUDu1" +
            "87mr/GUdeg76W2rV4Etf056rvFea0iGGXOsr12InFX/7CGAIT6kJRMPYUiibCMTlOFPMJjnLNtKiB" +
            "bg8esR3StY3xw6LjfcMibM+5KB4ewOAp4H4RGEoCNW8fPWFDyR136IbOQIcCpaZW9wspZpuuo+mgb" +
            "6Hk7XaZ47sHCGy7Ka41dqXJP8L1XxJKl8xIoHwiv8p6NNsuyEkhk37NSfkJD07mwYSUkNnX9c2ibR" +
            "o1N/NI2AbSkwTawJcI/HiVyI6+nKNSfG5JnKchJFnni+24TjfbjsUzvhysIQISopnO+2WbX3Qkwoj" +
            "kzCTP98SsUCgYEA9aPqWWtnKoxqO6Ey5LAQopK9MoNGvonETfI/GoQr/CLRBReIN5f8r5Zr/U92Gw" +
            "IYeF91ieOrZvFLISWs03mhd6U/4FTxWE9ZwhLllwj/L7exNJe4UFflgRzSH3EIvucXPifblgKWDfi" +
            "yEF9X+Rn25LzgKObsjP/u6wd1YYTnkYMCgYEAl6a5PgDxQl7EfazxS9HoEG/SViN35maqyeosd/Cw" +
            "QeTU3eMs6vOU8mEB4+1wHXmBe5zMrCSLwGV5otM0tHq6Q9ntdM8aQsQHI1EIiGIHyJnByWJUAzzfO" +
            "sOK3/RJt4+cBAE0JpwPQtO/yDgr28S1ThAo/aCOgAzqv+fcts+FYAMCgYEAxhd5HsF5ghuYdBwMQd" +
            "YmpCR4BStOusu5Di7kasrZ+cxOCbAvfr3H3gYNcktJcIBhIXnB/QAoMq02LDYIyMYWy7js6Uk3FEN" +
            "xnAvHMOTlfiLB+iq8TbaEc3m9jfOG3QTZfpt//tp5Di1X09I/zLf+nfofP3zymMawcsMiCxG1+WcC" +
            "gYAee2eem+filFPggwOZXt7Ldv6LS+PMzvQjU8uObDWxeQKEeyZLL5zPv0n2+4Hems64/wJw04O3r" +
            "23smZFteYG5I7PEF+DKbMaWhbI2X6X631UBWp9Sa0jfcfCXJW4v1FDiyS8nV5UFtLRG1WuTaA2dXE" +
            "gw+rXJ7U6+Nie6IypG6wKBgQCbc/dQiEu6rA15zBq5x7k3MYFjC+bzOf6e+lqdb0TxTjzEyOvkeyo" +
            "AUbYVk7EkBaVdw9M3P4fqDB7c9JTopH/EgoHbZCZ5SjKcGiWa9jrdnIo5Zhu+3eyuQhAVJYw0cg9S" +
            "UfMNn5I1pm7hxSyxyy2AhoME2/HV7a8rH7aKEIrPsA==";

    // 默认的Token超时时间
    public static final Integer DEFAULT_EXPIRE_DAY = 1;
}

