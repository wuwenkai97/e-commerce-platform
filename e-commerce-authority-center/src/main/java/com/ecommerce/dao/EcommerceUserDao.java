package com.ecommerce.dao;

import com.ecommerce.entity.EcommerceUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EcommerceUserDao extends JpaRepository<EcommerceUser, Long> {

    /**
     * 根据用户名查询EcommerceUser对象
     */
    EcommerceUser findByUsername(String username);

    /**
     * 根据用户名和密码查询实体对象
     */
    EcommerceUser findByUsernameAndPassword(String username, String password);

}
