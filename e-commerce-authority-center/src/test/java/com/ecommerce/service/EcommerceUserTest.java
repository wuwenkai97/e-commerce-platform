package com.ecommerce.service;

import cn.hutool.crypto.digest.MD5;
import com.ecommerce.dao.EcommerceUserDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * EcommerceUser相关的测试
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class EcommerceUserTest {

    @Autowired
    private EcommerceUserDao ecommerceUserDao;

    @Test
    public void createUserRecord() {
        // 测试插入功能
//        EcommerceUser ecommerceUser = new EcommerceUser();
//        ecommerceUser.setUsername("wen@163.com");
//        ecommerceUser.setPassword(MD5.create().digestHex("12345678"));
//        ecommerceUser.setExtraInfo("{}");
//        log.info("save user: [{}]", JSON.toJSONString(ecommerceUserDao.save(ecommerceUser)));

        // 测试查找功能
        log.info(ecommerceUserDao.findByUsername("wen@163.com").toString());
        log.info(ecommerceUserDao.findByUsernameAndPassword(
                "wen@163.com",
                MD5.create().digestHex("12345678")
        ).toString());
    }
}
