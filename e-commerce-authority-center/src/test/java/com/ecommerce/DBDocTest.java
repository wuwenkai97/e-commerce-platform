package com.ecommerce;

import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 数据库表文档生成
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class DBDocTest {

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * 配置想要生成的数据表和想要忽略的数据表
     * @return
     */
    private ProcessConfig getProduceConfig() {

        // 忽略的数据表
        List<String> ignoreTableName = Collections.singletonList("undo_log");

        // 忽略表前缀，忽略a、b开头的数据库表
        List<String> ignorePrefix = Arrays.asList("a", "b");

        // 忽略表后缀
        List<String> ignoreSuffix = Arrays.asList("_test", "_Test");

        return ProcessConfig.builder()
                // 根据表名生成文档
                .designatedTableName(Collections.emptyList())
                // 展昭前后缀生成
                .designatedTablePrefix(Collections.emptyList())
                .designatedTableSuffix(Collections.emptyList())
                .ignoreTableName(ignoreTableName)
                .ignoreTablePrefix(ignorePrefix)
                .ignoreTableSuffix(ignoreSuffix)
                .build();
    }

    /**
     * 生成文档
     */
    @Test
    public void buildDBDoc() {
        DataSource dataSource = applicationContext.getBean(DataSource.class);

        // 生成文件配置
        EngineConfig engineConfig = EngineConfig.builder()
                // 生成文件路径
                .fileOutputDir("D:\\Privacy\\Wen\\Learning\\Java\\e-commerce-platform")
                // 打开目录
                .openOutputDir(false)
                .fileType(EngineFileType.HTML)
                .produceType(EngineTemplateType.freemarker)
                .build();

        // 生成文档配置，包含自定义版本号、描述等等
        // 文件名: 数据库名_description_version.html
        Configuration config = Configuration
                .builder()
                .version("1.0.0")
                .description("e-commerce-platform")
                .dataSource(dataSource)
                .engineConfig(engineConfig)
                .produceConfig(getProduceConfig())
                .build();

        // 执行生成
        new DocumentationExecute(config).execute();
    }
}
