package com.ecommerce.partition;

import com.alibaba.fastjson.JSON;
import com.ecommerce.vo.WWKMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.binder.PartitionKeyExtractorStrategy;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * 自定义从 Message 中提取 partition key 的策略
 */
@Slf4j
@Component
public class WWKPartitionKeyExtractorStrategy implements PartitionKeyExtractorStrategy {

    @Override
    public Object extractKey(Message<?> message) {

        WWKMessage wwkMessage = JSON.parseObject(
                message.getPayload().toString(),
                WWKMessage.class
        );

        // 自定义提取key
        String key = wwkMessage.getProjectName();
        log.info("SpringCloud Stream WWK Partition Key: [{}]", key);

        return key;
    }

}
