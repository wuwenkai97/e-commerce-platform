package com.ecommerce.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 消息传递对象：SpringCloud Stream + Kafka
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WWKMessage {

    private Integer id;

    private String projectName;

    private String org;

    private String author;

    private String version;

    /**
     * 返回默认消息, 方便使用
     */
    public static WWKMessage defaultMessage() {
        return new WWKMessage(
                1,
                "e-commerce-stream-client",
                "baidu.com",
                "WWK",
                "1.0"
        );
    }

}
