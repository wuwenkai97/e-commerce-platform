package com.ecommerce.stream;

import com.alibaba.fastjson.JSON;
import com.ecommerce.vo.WWKMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * 使用默认的信道实现消息的接收
 */
@Slf4j
@EnableBinding(Sink.class) // 对输入通道的绑定
public class DefaultReceiveService {

    /**
     * 使用默认的输入信道接收消息
     */
    @StreamListener(Sink.INPUT)
    public void receiveMessage(Object payload) {
        log.info("in DefaultReceiveService consume message start");
        WWKMessage wwkMessage = JSON.parseObject(payload.toString(), WWKMessage.class);

        // 消费消息
        log.info("in DefaultReceiveService consume message success: [{}]",
                JSON.toJSONString(wwkMessage));

    }

}
