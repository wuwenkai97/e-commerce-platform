package com.ecommerce.stream.wwk;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * 自定义输出信道
 */
public interface WWKSource {

    String OUTPUT = "wwkOutput";

    /**
     * 输出信道的名称是 wwkOutput, 同时还需要 Stream 绑定器在 yml 文件中声明
     */
    @Output(WWKSource.OUTPUT)
    MessageChannel wwkOutput();

}
