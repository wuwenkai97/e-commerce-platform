package com.ecommerce.stream.wwk;

import com.alibaba.fastjson.JSON;
import com.ecommerce.vo.WWKMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

/**
 * 使用自定义的通信信道 WWKSource 实现消息的发送
 */
@Slf4j
@EnableBinding(WWKSource.class)
public class WWKSendService {

    private final WWKSource wwkSource;

    public WWKSendService(WWKSource wwkSource) {
        this.wwkSource = wwkSource;
    }

    /**
     * 使用自定义的输出信道发送消息
     */
    public void sendMessage(WWKMessage message) {
        String _message = JSON.toJSONString(message);
        log.info("in WWKSendService send message: [{}]", _message);
        wwkSource.wwkOutput().send(
                MessageBuilder.withPayload(_message).build()
        );
    }
}
