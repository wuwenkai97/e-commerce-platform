package com.ecommerce.stream.wwk;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * 自定义输入信道
 */
public interface WWKSink {

    String input = "wwkInput";

    /**
     * 输入信道的名称是 wwkInput, 需要使用Stream绑定器在 yml 文件中配置
     */
    @Input(input)
    SubscribableChannel wwkInput();

}
