package com.ecommerce.stream.wwk;

import com.alibaba.fastjson.JSON;
import com.ecommerce.vo.WWKMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;

/**
 * 使用自定义的输入信道实现消息的接收
 */
@Slf4j
@EnableBinding(WWKSink.class)
public class WWKReceiveService {

    /**
     * 使用自定义的输入信道接收消息
     */
    @StreamListener(WWKSink.input)
    public void receiveMessage(@Payload Object payload) {
        log.info("in WWKReceiveService consume message start");
        WWKMessage wwkMessage = JSON.parseObject(
                payload.toString(), WWKMessage.class
        );
        log.info("in WWKReceiveService consume message success: [{}]", JSON.toJSONString(wwkMessage));
    }

}
