package com.ecommerce.controller;

import com.ecommerce.stream.DefaultSendService;
import com.ecommerce.stream.wwk.WWKSendService;
import com.ecommerce.vo.WWKMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 构建消息驱动
 */
@RestController
@Slf4j
@RequestMapping("/message")
public class MessageController {

    private final WWKSendService wwkSendService;

    private final DefaultSendService defaultSendService;

    public MessageController(WWKSendService wwkSendService, DefaultSendService defaultSendService) {
        this.wwkSendService = wwkSendService;
        this.defaultSendService = defaultSendService;
    }

    @RequestMapping("/default")
    public void defaultSend() {
        defaultSendService.sendMessage(WWKMessage.defaultMessage());
    }

    /**
     * 自定义信道
     */
    @RequestMapping("/wwk")
    public void wwkSend() {
        wwkSendService.sendMessage(WWKMessage.defaultMessage());
    }
}
